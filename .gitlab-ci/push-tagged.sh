#!/bin/bash

set -e

#
# Push the Docker image to the Docker Hub using the Git tag.
#
# Parameters:
#
# $CI_COMMIT_SHA
# $CI_COMMIT_TAG
# $CI_REGISTRY
# $CI_REGISTRY_IMAGE
# $CI_REGISTRY_PASSWORD
# $CI_REGISTRY_USER
#
# $DOCKERHUB_USERNAME
# $DOCKERHUB_PASSWORD
# $DOCKERHUB_IMAGE_NAME
#
# @version 0.1.2
# @author David DIDIER
# @see https://gitlab.com/ddidier/docker-tools
#

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $SCRIPT_DIR/commons.sh

print_h1 "Push the Docker image to the Docker Hub using the Git tag"

print_h2 "Script parameters"
echo "CI_COMMIT_SHA = $CI_COMMIT_SHA"
echo "CI_COMMIT_TAG = $CI_COMMIT_TAG"
echo "CI_REGISTRY = $CI_REGISTRY"
echo "CI_REGISTRY_IMAGE = $CI_REGISTRY_IMAGE"
echo "CI_REGISTRY_PASSWORD = **********"
echo "CI_REGISTRY_USER = $CI_REGISTRY_USER"

# check_semantic_version "$CI_COMMIT_TAG"

print_h2 "Current Docker informations"
docker info

print_h2 "Authenticating against the GitLab registry"
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

# because we have no guarantee that this job will be picked up by the same runner
# that built the image in the previous step, we pull it again locally
print_h2 "Pulling the built image locally"
docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

print_h2 "Tagging the image with the Git tag '$CI_COMMIT_TAG'"
docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG

print_h2 "Pushing the image tagged with '$CI_COMMIT_TAG' to the GitLab registry"
docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG

print_h2 "Logging out from the GitLab registry"
docker logout

print_h2 "Authenticating against the Docker Hub registry"
docker login -u $DOCKERHUB_USERNAME -p $DOCKERHUB_PASSWORD

print_h2 "Tagging the image with the Git tag '$CI_COMMIT_TAG'"
docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $DOCKERHUB_IMAGE_NAME:$CI_COMMIT_TAG

print_h2 "Pushing the image tagged with '$CI_COMMIT_TAG' to the Docker Hub registry"
docker push $DOCKERHUB_IMAGE_NAME:$CI_COMMIT_TAG

print_h2 "Pushing the README to the Docker Hub registry"
token=$(get_token "$DOCKERHUB_USERNAME" "$DOCKERHUB_PASSWORD")
push_readme "$DOCKERHUB_IMAGE_NAME" "$token"
